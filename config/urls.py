"""Main URLs module."""

from django.conf import settings
from django.urls import include, path
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = [
    # Django Admin
    path(settings.ADMIN_URL, admin.site.urls),

    path('', include(('alquiler.clients.urls','clients'), namespace = 'clients')),
    path('', include(('alquiler.users.urls','users'), namespace = 'users')),
    path('', include(('alquiler.categories.urls','categories'), namespace = 'categories')),
    path('', include(('alquiler.machinery.urls','machinery'), namespace = 'machinery')),
    path('', include(('alquiler.rentals.urls','rentals'), namespace = 'rentals')),
    

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)