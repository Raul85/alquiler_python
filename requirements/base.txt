pytz==2020.5  
python-slugify==4.0.1  
Pillow==8.1.0  
argon2-cffi==20.1.0  
whitenoise==5.2.0  
redis==3.5.3  
hiredis==1.1.0  
celery==4.4.6  # pyup: < 5.0,!=4.4.7  
django-celery-beat==2.1.0  
django-timezone-field==4.0  
flower==0.9.7  

# Django
# ------------------------------------------------------------------------------
django==3.0.11  # pyup: < 3.1  
django-environ==0.4.5  
django-model-utils==4.1.1  
 
django-redis==4.12.1  

# JWT
pyjwt>=1.7.1

# Django REST Framework
djangorestframework==3.12.2  

