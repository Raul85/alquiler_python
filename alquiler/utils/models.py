"""Utilidades"""

# Django
from django.db import models

class DateModel(models.Model):
    """ campos para añadir hora de creacion y modificacion"""

    created = models.DateTimeField(
        'created at',
        auto_now_add=True,
        help_text='Fecha y hora de creacion'
    )

    modified = models.DateTimeField(
        'modified at',
        auto_now_add=True,
        help_text='Fecha y hora de modificacion'
    )

    class Meta:

        abstract=True,

        get_latest_by = 'created'
        ordering = ['-created','-modified']

