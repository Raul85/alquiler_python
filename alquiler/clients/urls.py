"""clients urls"""

# Django
from django.urls import include, path

# Django REST Framework
from rest_framework.routers import DefaultRouter

# views
from .views import clients as client_views

router = DefaultRouter()
router.register(r'clients', client_views.ClientViewSet, basename='client')

urlpatterns = [
    path('', include(router.urls))
]