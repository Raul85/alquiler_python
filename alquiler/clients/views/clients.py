"""Clients views"""

# Django REST Framework
from rest_framework import mixins, viewsets

# Serializer 
from alquiler.clients.serializers import ClientModelSerializer

# Models
from alquiler.clients.models import Client

class ClientViewSet(
        mixins.CreateModelMixin,
        mixins.DestroyModelMixin,
        mixins.UpdateModelMixin,
        mixins.ListModelMixin,
        viewsets.GenericViewSet):
    
    serializer_class = ClientModelSerializer
    lookup_field = 'nit'
    
    def get_queryset(self):
        """Listar clientes"""
        queryset = Client.objects.all()
        return queryset

   
    def perform_create(self, serializer):
        """Guardar cliente"""
        client = serializer.save()
    

    def perform_destroy(self, instance):
        """Eliminar cliente"""
        instance.delete()
