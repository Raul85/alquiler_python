"""Client model"""

# Django
from django.db import models
from django.core.validators import RegexValidator

# utilities
from alquiler.utils.models import DateModel

class Client(DateModel):
   
    client_id = models.AutoField(primary_key=True)
    nit = models.CharField(
        max_length=15,
        unique=True,
        error_messages={
            'unique': 'El nit ya existe'
        }
        
    )
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=50, blank=True)
    email = models.EmailField(blank=True)

    phone_regex = RegexValidator(
        regex=r'\d{9,10}$',
        message="El formato para el numero telefonico: 99999999 8 digitos"
    )
    
    phone = models.CharField(validators=[phone_regex], max_length=10, blank=True)

    def __str__(self):
      
        return '{} / {}'.format(
            self.nit,
            self.name
        )

