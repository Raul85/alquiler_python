"""Clients serializers"""

# Django REST Framework
from rest_framework import serializers
from django.core.validators import RegexValidator

#Models
from alquiler.clients.models import Client

class ClientModelSerializer(serializers.ModelSerializer):


    nit = serializers.CharField(max_length=15)
    name = serializers.CharField(max_length=50)
    address = serializers.CharField(max_length=50)
    email = serializers.EmailField()

    phone_regex = RegexValidator(
        regex=r'\d{8,10}$',
        message="El formato para el numero telefonico: 99999999 8 digitos"
    )
    
    phone = serializers.CharField(validators=[phone_regex], max_length=10)

    class Meta:

        model = Client
        fields = (
            'nit',
            'name',
            'address',
            'email',
            'phone'
        )

        