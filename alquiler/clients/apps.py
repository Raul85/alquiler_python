"""Clients app"""

#Django
from django.apps import AppConfig


class ClientsAppConfig(AppConfig):
    """Clients app config"""

    name = 'alquiler.clients'
    verbose_name = 'Clients'