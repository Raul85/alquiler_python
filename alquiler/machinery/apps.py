"""Machinery app"""

#Django
from django.apps import AppConfig


class MachineryAppConfig(AppConfig):
    """Machinery app config"""

    name = 'alquiler.machinery'
    verbose_name = 'Machinery'