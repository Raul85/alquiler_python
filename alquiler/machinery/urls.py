"""Machinery urls"""

# Django
from django.urls import include, path

# Django REST Framework
from rest_framework.routers import DefaultRouter

# views
from .views import machinery as machinery_views

router = DefaultRouter()
router.register(r'machinery', machinery_views.MachineryViewSet, basename='machinery')

urlpatterns = [
    path('', include(router.urls))
]