"""Machinery model"""

# Django
from django.db import models

# utilities
from alquiler.utils.models import DateModel

class Machinery(DateModel):
   
    sku = models.PositiveIntegerField(
        primary_key=True,
        unique=True,
        error_messages={
            'unique': 'La maquinaria ya existe'
        }
    )
    description = models.CharField(max_length=150)
    model = models.CharField(max_length=50)
    status = models.BooleanField(default=1)
    rental = models.BooleanField(default=0)
   
    category = models.ForeignKey(
        'categories.Category',
        help_text="Ingrese categoria de la maquina",
        on_delete=models.CASCADE
    )

    class Meta(DateModel.Meta):
        """Meta class"""

        ordering = ['-description']


    def __str__(self):

        return self.description
