"""Machinery views"""

# Django REST Framework
from rest_framework import mixins, viewsets

# Serializer 
from alquiler.machinery.serializers import MachineryModelSerializer

# Models
from alquiler.machinery.models import Machinery

class MachineryViewSet(
        mixins.CreateModelMixin,
        mixins.DestroyModelMixin,
        mixins.UpdateModelMixin,
        mixins.ListModelMixin,
        viewsets.GenericViewSet):
    
    serializer_class = MachineryModelSerializer
    lookup_field = 'sku'
    
    def get_queryset(self):
        """Listar maquinarias"""
        queryset = Machinery.objects.all()
        return queryset

   
    def perform_create(self, serializer):
        """Guardar maquinaria"""
        rent = serializer.save()

    

    def perform_destroy(self, instance):
        """Eliminar maquinaria"""
        instance.delete()
