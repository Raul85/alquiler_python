"""Machinery serializers"""

# Django REST Framework
from rest_framework import serializers

#Models
from alquiler.machinery.models import Machinery

class MachineryModelSerializer(serializers.ModelSerializer):


    description = serializers.CharField(max_length=15)
    model = serializers.CharField(max_length=50)
   
    class Meta:

        model = Machinery
        fields = (
            'sku',
            'description',
            'model',
            
        )

        