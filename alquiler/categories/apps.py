"""Categories app"""

#Django
from django.apps import AppConfig


class CategoriesAppConfig(AppConfig):
    """Categories app config"""

    name = 'alquiler.categories'
    verbose_name = 'Categories'