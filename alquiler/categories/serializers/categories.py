"""Categories serializers"""

# Django REST Framework
from rest_framework import serializers

#Models
from alquiler.categories.models import Category

class CategoryModelSerializer(serializers.ModelSerializer):

    description = serializers.CharField(max_length=50)

    class Meta:

        model = Category
        #fields = ( 'description', )
        fields = ( '__all__' )