"""Categories views"""

# Django REST Framework
from rest_framework import mixins, viewsets

# Serializer 
from alquiler.categories.serializers import CategoryModelSerializer

# Models
from alquiler.categories.models import Category

class CategoryViewSet(
        mixins.CreateModelMixin,
        mixins.DestroyModelMixin,
        mixins.UpdateModelMixin,
        mixins.ListModelMixin,
        viewsets.GenericViewSet):
    
    serializer_class = CategoryModelSerializer
    lookup_field = 'category_id'
    
    
    def get_queryset(self):
        """Listar categorias"""
        queryset = Category.objects.all()
        return queryset

   
    def perform_create(self, serializer):
        """Guardar categorias"""
        category = serializer.save()
    

    def perform_destroy(self, instance):
        """Eliminar categoria"""
        instance.delete()
