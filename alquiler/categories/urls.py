"""Categories urls"""

# Django
from django.urls import include, path

# Django REST Framework
from rest_framework.routers import DefaultRouter

# views
from .views import categories as categories_views

router = DefaultRouter()
router.register(r'categories', categories_views.CategoryViewSet, basename='categories')

urlpatterns = [
    path('', include(router.urls))
]