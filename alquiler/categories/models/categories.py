"""Categories model"""

# Django
from django.db import models

# utilities
from alquiler.utils.models import DateModel

class Category(DateModel):
   
    category_id = models.AutoField(primary_key=True)
    description = models.CharField(
        max_length=50,
        unique=True,
        error_messages={
            'unique': 'La categoria ya existe'
        }
    )
  
    def __str__(self):
      
        return self.description