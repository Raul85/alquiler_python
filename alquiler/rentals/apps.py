"""Rentals app"""

#Django
from django.apps import AppConfig


class RentalsAppConfig(AppConfig):
    """Rentals app config"""

    name = 'alquiler.rentals'
    verbose_name = 'Rentals'