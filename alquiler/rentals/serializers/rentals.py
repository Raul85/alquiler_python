"""Rentals serializers"""

# Django REST Framework
from rest_framework import serializers

#Models
from alquiler.rentals.models import Rental

class RentalModelSerializer(serializers.ModelSerializer):


    value = serializers.FloatField()
  
    class Meta:

        model = Rental
        fields = (
            'sku',
            'client',
            'value',
        )

        