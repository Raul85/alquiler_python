"""Rentals model"""

# Django
from django.db import models

# utilities
from alquiler.utils.models import DateModel

class Rental(DateModel):
   
    value = models.DecimalField(max_digits=10, decimal_places=2)

    client = models.ForeignKey(
        'clients.Client',
        help_text="Ingrese el cliente",
        on_delete=models.CASCADE
    )
    #sku = models.ForeignKey('machinery.Machinery', on_delete=models.CASCADE)
    sku = models.PositiveIntegerField
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    
    def __str__(self):

        return '{_machine} cliente {cl} | {value} | {user} '.format(
            _machine=self.sku_machinery.description,
            cl=self.client.name,
            value=str(self.value),
            user=self.user.username
        )
