"""Rentals views"""

# Django REST Framework
from rest_framework import mixins, viewsets

# Serializer 
from alquiler.rentals.serializers import RentalModelSerializer

# Models
from alquiler.rentals.models import Rental

class RentalViewSet(
        mixins.CreateModelMixin,
        mixins.DestroyModelMixin,
        mixins.UpdateModelMixin,
        mixins.ListModelMixin,
        viewsets.GenericViewSet):
    
    serializer_class = RentalModelSerializer
    #lookup_field = 'nit'
    
    def get_queryset(self):
        """Listar alquileres"""
        queryset = Rental.objects.all()
        return queryset

   
    def perform_create(self, serializer):
        """Guardar alquieres"""
        client = serializer.save()
    

    def perform_destroy(self, instance):
        """Eliminar alquileres"""
        instance.delete()
