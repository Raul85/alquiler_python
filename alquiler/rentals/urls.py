"""Rentals urls"""

# Django
from django.urls import include, path

# Django REST Framework
from rest_framework.routers import DefaultRouter

# views
from .views import rentals as rentals_views

router = DefaultRouter()
router.register(r'rentals', rentals_views.RentalViewSet, basename='rentals')

urlpatterns = [
    path('', include(router.urls))
]