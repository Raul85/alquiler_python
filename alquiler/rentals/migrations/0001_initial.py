# Generated by Django 3.0.11 on 2021-01-23 00:41

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('clients', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Rental',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, help_text='Fecha y hora de creacion', verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now_add=True, help_text='Fecha y hora de modificacion', verbose_name='modified at')),
                ('value', models.DecimalField(decimal_places=2, max_digits=10)),
                ('client', models.ForeignKey(help_text='Ingrese el cliente', on_delete=django.db.models.deletion.CASCADE, to='clients.Client')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-created', '-modified'],
                'get_latest_by': 'created',
                'abstract': False,
            },
        ),
    ]
