"""User model"""

# Django
from django.db import models
from django.contrib.auth.models import AbstractUser

# Utilidades
from alquiler.utils.models import DateModel

class User(DateModel, AbstractUser):
  
    email = models.EmailField(
        'email address',
        unique=True,
        error_messages={
            'unique': 'El correo ya existe'
        }
    )

    """cambio campo usuario por email"""
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    is_client = models.BooleanField(
        'client',
        default=True,
        help_text=(
            'Usuario cliente'
        )
    )

    is_active = models.BooleanField(
        'status',
        default=True,
        help_text=(
            'Usuario activo'
        )
    )

    is_verified = models.BooleanField(
        'verified',
        default=True,
        help_text='Correo de usuario verificado'
    )

    def __str__(self):
        return self.username

    def get_short_name(self):
        """Retorna username"""
        return self.username
