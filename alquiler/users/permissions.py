"""User permissions"""

# Django REST Framework
from rest_framework.permissions import BasePermission

class IsAccountOwner(BasePermission):
    """permitir acceso solamente a los objetos"""   
   
    def has_object_permission(self, request, view, obj):
        """comprobar que el objeto y usuario sean iguales"""
        return request.user == obj