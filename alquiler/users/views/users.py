"""users views"""

# Django REST framework
from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response 

# Permissions
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated
)
from alquiler.users.permissions import IsAccountOwner

# Serializer
from alquiler.users.serializers import (
    UserLoginSerializer,
    UserModelSerializer,
    UserSignUpSerializer,
    AccountVerificationSerializer
)

# Models 
from alquiler.users.models import User

class UserViewSet(mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  viewsets.GenericViewSet):
    """User view ser"""
    
    queryset = User.objects.filter(is_active=True, is_client=True)
   
    def get_permissions(self):
        """Asignar permisos basados en la accion"""

        if self.action in ['signup','login','verify']:
            permissions = [AllowAny]
        elif self.action in ['retrieve','update','partial_update']:
            permissions = [IsAuthenticated, IsAccountOwner]
        else:
            permissions = [IsAuthenticated]
        return [p() for p in permissions]


    @action(detail=False, methods=['post'])
    def login(self, request):
        """user sign in (Registrarse)"""

        serializer = UserLoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user, token = serializer.save()
        data = {
            'user': UserModelSerializer(user).data,
            'access_token': token
        }
        return Response(data, status=status.HTTP_201_CREATED)


    @action(detail=False, methods=['post'])
    def signup(self, request):
        """ user sign up"""

        serializer = UserSignUpSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        data = UserModelSerializer(user).data
        return Response(data, status=status.HTTP_201_CREATED)


    @action(detail=False, methods=['post'])
    def verify(self, request):
        """Verificar cuenta"""

        serializer = AccountVerificationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        data = {'message': 'Felicidades, tu cuenta ha sido activada'}
        return Response(data, status=status.HTTP_200_OK)


 