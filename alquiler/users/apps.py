"""Users app"""

# Django
from django.apps import AppConfig


class UsersAppConfig(AppConfig):
    """Users app config"""

    name = 'alquiler.users'
    verbose_name = 'Users'