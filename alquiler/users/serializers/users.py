"""Users serializer"""

# Django
from django.conf import settings
from django.contrib.auth import password_validation, authenticate
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils import timezone

# Django REST framework
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework.validators import UniqueValidator

# Models
from alquiler.users.models import User

# Utilities
import jwt
import time
from datetime import timedelta


class UserModelSerializer(serializers.ModelSerializer):
    """user model serializer"""
   
    class Meta:

        model = User
        fields = (
            'username',
            'email'
        )

class UserSignUpSerializer(serializers.Serializer):
    
    email = serializers.EmailField(
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    username = serializers.CharField(
        min_length=4,
        max_length=20,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )

    password = serializers.CharField(min_length=8, max_length=64)
    password_confirmation = serializers.CharField(min_length=8, max_length=64)

    def validate(self, data):
        """Validar que las dos contraseñas sean iguales"""
       
        passWd = data['password']
        passWd_conf = data['password_confirmation']
        if passWd != passWd_conf:
            raise serializers.ValidationError('Contraseñas son distintas')
      
        password_validation.validate_password(passWd)
        return data


    def create(self, data):
        data.pop('password_confirmation')
        user = User.objects.create_user(**data, is_verified=False, is_client=True)
        self.send_confirmation_email(user)

        return user
    

    def send_confirmation_email(self, user):
        """Envio de link para verficacion de usuario"""
        
        verification_token = self.gen_verification_token(user)
        subject = 'Bienvenido @{}! Verificar tu cuenta para utilizar la aplicacion '.format(user.username)
        from_email = 'MultiAlquileres <noreply>@multialquileres.com'
        content = render_to_string(
            'emails/users/account_verification.html',
            {'token': verification_token, 'user':user}
        )
        msg = EmailMultiAlternatives(subject,content,from_email,[user.email])
        msg.attach_alternative(content, "text/html")
        msg.send()

    
    def gen_verification_token(self, user):
        """Create JWT token"""
        exp_date = timezone.now() + timedelta(days=3)
        payload = {
            'user': user.username,
            'exp': int(exp_date.timestamp()),
            'type': 'email_confirmation'
        }
        token = jwt.encode(payload, settings.SECRET_KEY, algorithm='HS256')
        return token

    
class UserLoginSerializer(serializers.Serializer):
    
    email = serializers.EmailField()
    password = serializers.CharField(min_length=8, max_length=64)

    def validate(self, data):
        """Revisar credenciales"""
        
        user = authenticate(username=data['email'], password=data['password'])
        if not user:
            raise serializers.ValidationError('Credenciales inválidas')

        if not user.is_verified:
            raise serializers.ValidationError('Cuenta no activa')

        self.context['user'] = user
        return data

    def create(self, data):
        """Generar o recuperar token"""
        token, created = Token.objects.get_or_create(user=self.context['user'])
        return self.context['user'], token.key


class AccountVerificationSerializer(serializers.Serializer):
    """Account verification serializer"""
   
    token = serializers.CharField()

    def validate_token(self, data):
        """verificar si el token es valido"""
     
        try:
            payload = jwt.decode(data, settings.SECRET_KEY, algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            raise serializers.ValidationError('Link de verificación ya expiró')
        except jwt.PyJWTError:
            raise serializers.ValidationError('Token inválido')

        if payload['type'] != 'email_confirmation':
            raise serializers.ValidationError('Token inválido')

        self.context['payload'] = payload
        return data

    def save(self):
        """Actualizar status vefificado"""
        
        payload = self.context['payload']
        user = User.objects.get(username=payload['user'])
        user.is_verified = True
        user.save()